package babysitter_kata;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.time.DateTimeException;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class BabySitterPayCalculatorTests
{
	// Test variable combinations.
	private static Stream familyAstartAndEndTimesAndPayForTesting()
	{
		return Stream.of(	Arguments.of("5pm", "11pm", 90),
							Arguments.of("5pm", "10pm", 75),
							Arguments.of("5 p.m.", "12 a. m.",110),
							Arguments.of("5 p.m.", "1 a. m.",130),
							
							Arguments.of("6pm", "10pm", 60),
							Arguments.of("6pm", "11pm", 75),
							Arguments.of("6 p.m.", "12 a. m.",95),
							Arguments.of("6p.m.", "1 a. m.",115), 
							Arguments.of("6 p.m.", "4 a. m.",175), 

							Arguments.of("11 p.m.", "4 a. m.",100), 
							Arguments.of("11 p.m.", "12 a. m.",20), 
							Arguments.of("11 p.m.", "1 a. m.",40), 

							
							Arguments.of("12am", "4am",80), 							Arguments.of("12am", "4am",80), 
							Arguments.of("12am", "1am",20), 

							Arguments.of("1am", "4am",60), 
							Arguments.of("1am", "3am",40), 
							Arguments.of("5pm", "4am",190));
							}
	private static Stream familyBValidstartAndEndTimesWithCorrectPay()
	{
		return Stream.of(	Arguments.of("5pm", "8pm", 36),
							Arguments.of("5pm", "10pm", 60),
							Arguments.of("5pm", "11pm", 68),
							Arguments.of("5pm", "12am", 76),
							Arguments.of("6 p.m.", "8 p. m.",24) , 
							Arguments.of("6pm", "10pm",48),
							Arguments.of("8pm", "11pm", 32),
							Arguments.of("8pm", "12am", 40),
							Arguments.of("10pm", "11pm",8), 
							Arguments.of("10pm", "12am", 16),
							Arguments.of("10pm", "1am", 32),
							Arguments.of("10pm", "4am", 80),
							Arguments.of("11pm", "12am",8),
							Arguments.of("11pm", "1am", 24),
							Arguments.of("11pm", "4am", 72),
							Arguments.of("12am", "3am",48),
							Arguments.of("12am", "4am", 64),
							Arguments.of("1am", "3am",32),
							Arguments.of("1am", "4am",48),
							Arguments.of("5pm", "4am",140)
							);
							}
	private static Stream familyCValidstartAndEndTimesWithCorrectPay()
	{
		return Stream.of(	Arguments.of("5pm", "9pm", 84),
							Arguments.of("5pm", "8pm", 63),
							Arguments.of("6pm", "8pm", 42),
							Arguments.of("6pm", "10pm", 78),
							Arguments.of("6pm", "12am", 108),
							Arguments.of("6pm", "1am", 123),
							Arguments.of("6pm", "4am", 168),
							Arguments.of("9 p.m.", "11 p. m.",30),
							Arguments.of("9pm", "12am",45), 
							Arguments.of("9pm", "1am",60),
							Arguments.of("9pm", "4am",105),
							Arguments.of("10pm", "11pm",15),
							Arguments.of("10pm", "12am",30),
							Arguments.of("10pm", "1am",45),
							Arguments.of("10pm", "4am",90),
							Arguments.of("12am", "3am",45),
							Arguments.of("1am", "3am",30),
							Arguments.of("1am", "4am",45));
							}
	/*
	 * First business rule: Babysitter can only work from 5pm to 4am. 
	 * 						End time must be later than start time.
	 * 						(Time is rounded up to the nearest hour.)
	 */
	@ParameterizedTest
	@ValueSource(strings = {" 1 am", "3am", "6PM", "12a.m.", "10 p. m."})
	void validTimeIs12HourFormat(String time)
	{
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		boolean result = calculator.isTimeValid(time);
		assertTrue(result);
	}
	@ParameterizedTest
	@ValueSource(strings = {"", " ", "0am", "18", "am","p.m.", "12k.m.", "14 p. m."})
	void invalidTimeThrowsDateTimeException(String time) {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.isTimeValid(time);});
	}
	@Test
	void timeBefore5pmIsInvalid() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.isTimeValid("4pm");});
	}
	@Test
	void timeAfter4amIsInvalid() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.isTimeValid("6am");});
	}
	/*
	 * When test values 5pm, 12am, 3am were added,
	 * the test passed.  So, I'm not sure what TDD
	 * procedure is.  Should the new test values not
	 * be tested because they didn't produce a red?
	 * Is it okay to leave them in?
	 * 
	 * I've decided to leave them because they are edge 
	 * cases.
	 */
	@Test
	void validStartTimeIsSetTo24HourFormat() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		calculator.setStartTime("5pm");
		int result = calculator.getStartTime();
		assertTrue(result == 17);
		result = calculator.getStartTime();
		calculator.setStartTime("6pm");
		result = calculator.getStartTime();
		assertTrue(result == 18);
		calculator.setStartTime("12am");
		result = calculator.getStartTime();
		assertTrue(result == 24);
		calculator.setStartTime("3am");
		result = calculator.getStartTime();
		assertTrue(result == 3);
		
		
	}
	@Test
	void valid24HourTimeConvertsTo12HourFormat() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		String formatedStartTime = calculator.convert24HourTimeTo12Hour(17);
		assertTrue(formatedStartTime.equals("5PM"));
		formatedStartTime = calculator.convert24HourTimeTo12Hour(4);
		assertTrue(formatedStartTime.equals("4AM"));
	}
	@Test
	void whenConvertingInvalid24HourTimeTo12HourFormatThrowsDateTimeException() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.convert24HourTimeTo12Hour(25);});
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.convert24HourTimeTo12Hour(0);});
	}
	@Test
	void invalidStartTimeThrowsDateTimeException() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.setStartTime("6am");});
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.setStartTime("4am");});
		
	} 
	@Test
	void validEndTimeIsSetTo24HourFormat() throws IOException {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		calculator.setStartTime("5pm");   	//Added to avoid throwing IOException.
		calculator.setEndTime("10pm");
		int result = calculator.getEndTime();
		assertTrue(result == 22);
		calculator.setEndTime("12am");
		result = calculator.getEndTime();
		assertTrue(result == 24);
	}
	/*
	 * A red state wasn't met when adding 5am and 12am.
	 * Not sure if according to TDD if these values should
	 * be kept in the test.
	 * 
	 * For now, I'm keeping them in because they are edge cases.
	 */
	void invalidEndTimeThrowsDateTimeException() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.setEndTime("4pm");});
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.setEndTime("5am");});
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.setEndTime("12pm");});
		Assertions.assertThrows(DateTimeException.class, () -> {
		    calculator.setEndTime("5pm");});
		
	} 
	/*
	 * I added second assert to test when end time = start time.
	 * It passed without going through red phase.
	 * I'm keeping the second assert because if I accidently
	 * change if(endTime<=startTime) to if(endTime<startTime), or
	 * something else.  This test will catch it.
	 */
	@Test
	void whenEndTimeIsSetBeforeStartTimeIOExceptionIsThrown() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		calculator.setStartTime("10pm");
		Assertions.assertThrows(IOException.class, () -> {
		    calculator.setEndTime("6pm");});
	}
	@Test
	void whenEndTimeEqualsStartTimeIOExceptionIsThrown() {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		calculator.setStartTime("10pm");
		Assertions.assertThrows(IOException.class, () -> {
		    calculator.setEndTime("10pm");});
		
	}
	/*
	 * Second business rule: 
	 * Babysitter are paid as follows:
	 * 
	 * Family A pays:	$15 per hour before 11pm, and 
	 * 					$20 per hour the rest of the night
	 * 
	 * Family B pays:	$12 per hour before 10pm, 
	 * 					$8 between 10 and 12, and 
	 * 					$16 the rest of the night
	 *
	 * Family C pays:	$21 per hour before 9pm, then 
	 * 					$15 the rest of the night
	 * 
	 * Note: Babysitter can only work for one family a night.
	 */
	
	@Test
	void validfamiliesAreAorBorC() throws IOException {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		calculator.setFamily('A');
		char result = calculator.getFamily();
		assertTrue(result=='A');
		
		calculator.setFamily('B');
		result = calculator.getFamily();
		assertTrue(result=='B');
		
		calculator.setFamily('C');
		result = calculator.getFamily();
		assertTrue(result=='C');
	}
	@ParameterizedTest
	@ValueSource(chars = {'Z', '4', '@'})
	void whenInvalidFamilyEnteredIOExceptionThrown(char family) {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		Assertions.assertThrows(IOException.class, () -> {
			calculator.setFamily(family);});
	}
	
	@ParameterizedTest
	@MethodSource("familyAstartAndEndTimesAndPayForTesting")
	void familyAcalculatesPaysCorrectly(String startTime, String endTime, int pay) throws IOException {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		calculator.setFamily('A');
		calculator.setStartTime(startTime);
		calculator.setEndTime(endTime);
		int calculatedPay = calculator.calculatePay();
		assertTrue(calculatedPay==pay);
	}
	/*
	 * When redoing familyBcalculatesPayCorrectly,
	 * 	started by writing a list of all values
	 *  to be tested, then went through them one by one.
	 *  After coding familyCcalculatesPayCorrectly,
	 *  I felt like I had pushed the coding for familyB
	 *  past TDD guidelines, so I wanted to try again and
	 *  see if I didn't write better code doing that.
	 */
	@ParameterizedTest
	@MethodSource("familyBValidstartAndEndTimesWithCorrectPay")
	void familyBcalculatesPayCorrectly(String startTime, String endTime, int pay) throws IOException {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		calculator.setFamily('B');
		calculator.setStartTime(startTime);
		calculator.setEndTime(endTime);
		int calculatedPay = calculator.calculatePay();
		assertTrue(calculatedPay==pay);
	}
	@ParameterizedTest
	@MethodSource("familyCValidstartAndEndTimesWithCorrectPay")
	void familyCcalculatesPayCorrectly(String startTime, String endTime, int pay) throws IOException {
		BabySitterPayCalculator calculator = new BabySitterPayCalculator();
		calculator.setFamily('C');
		calculator.setStartTime(startTime);
		calculator.setEndTime(endTime);
		int calculatedPay = calculator.calculatePay();
		assertTrue(calculatedPay==pay);
	}

}

/*
 * Learning notes:
 * -Choosing Test Values:
 * 		--Make sure testing values are complete, including all edge cases.
 * 			(i.e. 	earliest start time to first pay change, 
 * 					earliest start time +1 to first pay change,
 * 					within first pay rate,
 * 					time within first pay rate to midnight (if applicable),
 * 					time within first pay rate to past midnight (if applicable),
 * 					time within first pay rate to time in next pay rate,
 * 					repeat for time boundaries for time boundaries of next pay rate,
 * 					earliest start time to latest start time.) 
 * -I wonder if it wouldn't have been easier if I had started with a SimpleDateFormat
 *  for the time and it would have been easier to compare time when I got to 
 *  calculating pay.  However, TDD didn't point me in that direction or at least 
 *  not in the beginning.  Not sure if it would be valid under TDD guidelines to 
 *  refactor start and end times now.  I'm also not sure it would improve readability or maintenance.
 * 
 /*
 * Ideas:
 * Write automatic parameter list generator that automatically
 * populates private static stream variables for MethodSource
 * with static variables EARLIEST_START_TIME, LATEST_END_TIME,
 * FAMILY_A_FIRST_TRANSITION_TIME, FAMILY_B_SECOND_TRANSITION_TIME, etc.
 * first try at logic:
 * Save array of transition times for each family
 * Get number of Transitions
 * Earliest startTime to midnight.
 * midnight to latest starttime.
 * earliest startTime to latest start time.
 * For(int x = 0; x< number of transitions; ++x)
 *  Earliest start time to transition[i]
 *  
 *  
 * 	For(int y = 1; y<=number of transtions; ++y)
 * 		{	
 * 			if(transition <= midnight)
 *				earliest to transition[i]
 *				earliest to transition[i]-1
 *				earliest + 1 to transtion[i]
 *				earliest + 1 to transition[i] -1
 *			else
 *				earliest to 
 *
 *		
 *			
 * 		}
 * 		if(transition
 * 
 * Refactor valid families A B and C to be more dynamic.
 */

