package Kata;

import java.io.IOException;
import java.time.DateTimeException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BabySitterPayCalculator
{
	private static final int EARLIEST_START_TIME = 17;
	private static final int lATEST_END_TIME = 4;
	private static final int MIDNIGHT = 24;

	private static final int FAMILY_A_FIRST_TRANSITION_HOUR = 23;
	private static final int FAMILY_B_FIRST_TRANSITION_HOUR = 22;
	private static final int FAMILY_B_SECOND_TRANSITION_HOUR = 24;
	private static final int FAMILY_C_FIRST_TRANSITION_HOUR = 21;

	private static final int FAMILY_A_FIRST_PAY_RATE = 15;
	private static final int FAMILY_A_SECOND_PAY_RATE = 20;
	private static final int FAMILY_B_FIRST_PAY_RATE = 12;
	private static final int FAMILY_B_SECOND_PAY_RATE = 8;
	private static final int FAMILY_B_THIRD_PAY_RATE = 16;
	private static final int FAMILY_C_FIRST_PAY_RATE = 21;
	private static final int FAMILY_C_SECOND_PAY_RATE = 15;

	private int startTime;
	private int endTime;
	private char family;

	public int calculatePay()
	{
		switch (family)
		{
		case 'A':
			return calculateFamilyApay();
		case 'B':
			return calculateFamilyBpay();
		case 'C':
			return calculateFamilyCpay();

		default:
			return 0;
		}
	}

	public int calculateFamilyApay()
	{
		// Calculate pay between first transition hours and latest end time.
		if (startTime <= lATEST_END_TIME) return FAMILY_A_SECOND_PAY_RATE * (endTime - startTime);
		if (startTime == MIDNIGHT)
			return FAMILY_A_SECOND_PAY_RATE * endTime;

		// Calculate pay between earliest start time and latest end time.
		else if (startTime >= EARLIEST_START_TIME)
			// End time is between midnight and latest end time.
			if (endTime <= lATEST_END_TIME)
			return FAMILY_A_FIRST_PAY_RATE * (FAMILY_A_FIRST_TRANSITION_HOUR - startTime) + FAMILY_A_SECOND_PAY_RATE + (FAMILY_A_SECOND_PAY_RATE * endTime);
			else if (endTime < MIDNIGHT)
			return FAMILY_A_FIRST_PAY_RATE * (endTime - startTime);
			else if (endTime == MIDNIGHT) return FAMILY_A_FIRST_PAY_RATE * (FAMILY_A_FIRST_TRANSITION_HOUR - startTime) + FAMILY_A_SECOND_PAY_RATE * (MIDNIGHT - FAMILY_A_FIRST_TRANSITION_HOUR);
		return 0;
	}

	public int calculateFamilyBpay()
	{
		// Calculate pay between 2nd transition hour and latest end time.
		if (startTime <= lATEST_END_TIME) return FAMILY_B_THIRD_PAY_RATE * (endTime - startTime);
		if (startTime >= FAMILY_B_SECOND_TRANSITION_HOUR) return FAMILY_B_THIRD_PAY_RATE * endTime;

		// Calculate pay between first transition hours and latest end time.
		if (startTime >= FAMILY_B_FIRST_TRANSITION_HOUR)
			if (endTime <= lATEST_END_TIME)
				return FAMILY_B_SECOND_PAY_RATE * (MIDNIGHT - startTime) + FAMILY_B_THIRD_PAY_RATE * endTime;
			else
				return FAMILY_B_SECOND_PAY_RATE * (endTime - startTime);

		// Calculate pay between earliest start time and latest end time.
		else if (startTime >= EARLIEST_START_TIME) if (endTime <= lATEST_END_TIME)
			return FAMILY_B_FIRST_PAY_RATE * (22 - startTime) + (FAMILY_B_THIRD_PAY_RATE * (1 + endTime));
		else if (endTime <= FAMILY_B_FIRST_TRANSITION_HOUR)
			return FAMILY_B_FIRST_PAY_RATE * (endTime - startTime);
		else if (endTime <= 24) return FAMILY_B_FIRST_PAY_RATE * (FAMILY_B_FIRST_TRANSITION_HOUR - startTime) + FAMILY_B_SECOND_PAY_RATE * (endTime - 22);
		return 0;
	}

	public int calculateFamilyCpay()
	{
		// Calculate pay between midnight and latest end time.
		if (startTime <= lATEST_END_TIME) return FAMILY_C_SECOND_PAY_RATE * (endTime - startTime);
		if (startTime == MIDNIGHT) return FAMILY_C_SECOND_PAY_RATE * endTime;

		// Calculate pay between first transition hours and latest end time.
		if (startTime >= FAMILY_C_FIRST_TRANSITION_HOUR)
			if (endTime <= lATEST_END_TIME)
				return FAMILY_C_SECOND_PAY_RATE * ((MIDNIGHT - startTime) + endTime);
			else
				return FAMILY_C_SECOND_PAY_RATE * (endTime - startTime);

		// Calculate pay between earliest start time and latest end time.
		else if (startTime >= EARLIEST_START_TIME) if (endTime <= lATEST_END_TIME)
			return FAMILY_C_FIRST_PAY_RATE * (FAMILY_C_FIRST_TRANSITION_HOUR - startTime) + 15 * ((24 - FAMILY_C_FIRST_TRANSITION_HOUR) + endTime);
		else if (endTime <= FAMILY_C_FIRST_TRANSITION_HOUR)
			return FAMILY_C_FIRST_PAY_RATE * (endTime - startTime);
		else if (endTime <= MIDNIGHT) return FAMILY_C_FIRST_PAY_RATE * (FAMILY_C_FIRST_TRANSITION_HOUR - startTime) + 15 * (endTime - FAMILY_C_FIRST_TRANSITION_HOUR);
		return 0;
	}

	public void setStartTime(String time)
	{
		if (isTimeValid(time))
		{
			int hour = extractHourFrom12HourTime(time);
			String meridian = extractMeridianFrom12HourTime(time);

			if (hour >= (EARLIEST_START_TIME - 12) && meridian.equals("PM") || hour == 12 && meridian.equals("AM"))
				this.startTime = hour + 12;
			else if (hour < lATEST_END_TIME && meridian.equals("AM"))
				this.startTime = hour;
			else
				throw new DateTimeException("Start must be 5pm or later and before 4am.");
		}
	}

	public void setEndTime(String time) throws IOException
	{
		if (isTimeValid(time))
		{

			int startTime = getStartTime();
			int hour = extractHourFrom12HourTime(time);
			int tempEndTime = 0;
			String meridian = extractMeridianFrom12HourTime(time);

			if (hour == 12)
				tempEndTime = MIDNIGHT;
			else if (hour > (EARLIEST_START_TIME - 12) && meridian.equals("PM"))
				tempEndTime = hour + 12;
			else if (hour <= lATEST_END_TIME && meridian.equals("AM"))
				tempEndTime = hour;
			else
				throw new DateTimeException("End time must be after 5pm and 4am or earlier.");

			if ((startTime >= EARLIEST_START_TIME && tempEndTime > EARLIEST_START_TIME && tempEndTime > startTime) || (startTime < lATEST_END_TIME && tempEndTime <= lATEST_END_TIME && tempEndTime > startTime) || (startTime >= EARLIEST_START_TIME && tempEndTime <= lATEST_END_TIME))
				this.endTime = tempEndTime;
			else
				throw new IOException("End time must be after start time.");
		}
	}

	public boolean isTimeValid(String time)
	{
		time = time.replace(" ", "");
		if (time.equals("")) throw new DateTimeException("Time is empty.");

		int hour = extractHourFrom12HourTime(time);
		String meridian = extractMeridianFrom12HourTime(time);

		if (hour > 12 || hour == 0) throw new DateTimeException("Hour is not valid.");

		if (hour == 12) if (meridian.equals("PM"))
			throw new DateTimeException("Time may not be before 5pm.");
		else
			return true;
		if (hour < (EARLIEST_START_TIME - 12) && meridian.equals("PM")) throw new DateTimeException("Time may not be before 5pm.");

		if (hour > lATEST_END_TIME && hour < 12 && meridian.equals("AM")) throw new DateTimeException("Time may not be after 4am.");
		return true;
	}

	public int extractHourFrom12HourTime(String time)
	{
		time = time.replace(" ", "");

		Pattern hourPattern = Pattern.compile("[0-9]{1,2}");
		Matcher hourMatch = hourPattern.matcher(time);

		if (hourMatch.find())
			return Integer.parseInt(hourMatch.group());
		else
			throw new DateTimeException("Hour is missing.");

	}

	public String extractMeridianFrom12HourTime(String time)
	{
		time = time.replace(" ", "");
		String tempMeridian = "";

		Pattern meridianPattern = Pattern.compile("[aApP]{1}\\.?[mM]{1}\\.?");
		Matcher meridianMatch = meridianPattern.matcher(time);

		if (!meridianMatch.find())
			throw new DateTimeException("Missing AM or PM.");
		else
			tempMeridian = meridianMatch.group().toUpperCase();

		if (tempMeridian.contains("A"))
			return "AM";
		else
			return "PM";
	}

	public int getStartTime()
	{
		return this.startTime;
	}

	public int getEndTime()
	{
		return this.endTime;
	}

	public char getFamily()
	{
		return this.family;
	}

	public void setFamily(char family) throws IOException
	{
		if (family == 'A' || family == 'B' || family == 'C')
			this.family = family;
		else
			throw new IOException("\nFamily is not valid.\nValid families are: A, B or C.");
	}

	public String convert24HourTimeTo12Hour(int time)
	{
		if(time>24|| time<=0)
			throw new DateTimeException("Hour for 24 hour format must be between 1 and 24.");
		if(time>12)
			return Integer.toString((time-12)) + "PM";
		else
			return Integer.toString(time)+"AM";
	}

	public static void main(String[] args)
	{
	Scanner keyboard = new Scanner(System.in);
	BabySitterPayCalculator calculator = new BabySitterPayCalculator();
	
	char family = ' ';
	boolean validFamily;
	do
	{
		validFamily = true;
		try{
			System.out.println("Please enter a family:");
			family = keyboard.nextLine().toUpperCase().charAt(0);
			calculator.setFamily(family);
		}catch(IOException e){
			validFamily = false;
			System.out.println(e.getMessage());
		}	
	}while(!validFamily);
	
	String startTime;
	boolean validStartTime;
	do {
		validStartTime = true;
		try {
			System.out.println("Enter start time:");
			startTime = keyboard.nextLine();
			calculator.setStartTime(startTime);
		}catch(DateTimeException e) {
			System.out.println(e.getMessage());
			validStartTime = false;
		}
	}while(!validStartTime);

	String endTime;
	boolean validEndTime;
	do {
		validEndTime = true;
		try {
			System.out.println("Enter end time:");
			endTime = keyboard.nextLine();
			calculator.setEndTime(endTime);
		}catch(DateTimeException | IOException e) {
			System.out.println(e.getMessage());
			validEndTime = false;
		}
	}while(!validEndTime);
	
	int pay = calculator.calculatePay();
	int startTime24HourFormat = calculator.getStartTime();
	int endTime24HourFormat = calculator.getEndTime();
	String startTimeForPrint = calculator.convert24HourTimeTo12Hour(startTime24HourFormat);
	String endTimeForPrint = calculator.convert24HourTimeTo12Hour(endTime24HourFormat);
	
	System.out.println("Babysitter worked for Family " + family);
	System.out.println("from " + startTimeForPrint + " to " + endTimeForPrint);
	System.out.println("and earned $" + pay + " dollars for the night.");


	}

}
